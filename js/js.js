function initDayNight() {
    // Init globals
    day = true;
    
    body = document.body;
    menu = document.getElementById('menu');
    header = document.getElementById('header');
    main = document.getElementById('main');
    footer = document.getElementById('footer');
    services1 = document.getElementById('services1');
    services2 = document.getElementById('services2');
    
    logo = document.getElementById('logo');

    // Add the onclick function
    logo.onclick = function(){
        if (day) {
            nm();
        } else {
            dm();
        }
    }
}

function nm() {
    day = false;
    menu.className = "nm";
    body.className = "nm";
    header.className = "nm";
    main.className = "nm";
    footer.className = "nm";
    services1.className = "nm";
    services2.className = "nm";
}

function dm() {
    day = true;
    menu.className = "";
    body.className = "";
    header.className = "";
    main.className = "";
    footer.className = "";
    services1.className = "";
    services2.className = "";
}


/*window.onload = function() { 
    initDayNight();
}
*/
// Side navigation
function w3_open() {
    var x = document.getElementById("mySidebar");
    //x.style.width = "100%";
    x.style.fontSize = "1.5em";
    x.style.display = "block";
}
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
}
